﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(HitObject))]
[CanEditMultipleObjects]
public class HitObject_Editor : Editor {


	private HitObject hitobject;
	private Transform Handletransform;
	private Quaternion Handlerotation;

	private void OnSceneGUI()
	{

		hitobject = target as HitObject;
		Handletransform = hitobject.transform;
		Handlerotation = Tools.pivotRotation == PivotRotation.Local ? Handletransform.rotation : Quaternion.identity;

		Handles.color = Color.yellow;
		Handles.DrawWireDisc(hitobject.transform.position,Vector3.back,hitobject.ScaledRadius);
					

	}




	public override void OnInspectorGUI (){


		DrawDefaultInspector();


	}



}
