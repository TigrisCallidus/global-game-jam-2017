﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour
{

	public int startGameAfterSeconds = 3;

	// Use this for initialization
	void Start ()
	{
		Invoke ("StartGame", startGameAfterSeconds);
	}
	
	// Update is called once per frame
	void StartGame ()
	{
		GameLogic.Instance.startNextLevel ();
	}
}

