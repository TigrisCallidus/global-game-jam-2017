﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSoundScript : MonoBehaviour {

	public AudioClip[] randomClips;
	public AudioSource UsedAudioSource;
	// Use this for initialization
	void Start () {
		int i = Random.Range(0, this.randomClips.Length);
		this.UsedAudioSource.PlayOneShot(this.randomClips[i]);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
