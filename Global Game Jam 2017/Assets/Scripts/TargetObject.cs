﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetObject : MonoBehaviour {

    public bool doRotate;
    public float rotationSpeed;
    private float startHitTime;

    private bool isHited;
    private Animator animatorController;
    private AudioSource hitAudioSource;
    public AudioClip[] randomClips;

    public TargetObject()
    {
        this.doRotate = true;
        this.rotationSpeed = 1f;

        this.isHited = false;
        this.animatorController = null;
        this.hitAudioSource = null;
    }
    
	void OnEnable () {
        if (this.animatorController == null)
            this.animatorController = this.gameObject.GetComponent(typeof(Animator)) as Animator;

        if (this.hitAudioSource == null)
            this.hitAudioSource = this.gameObject.GetComponent(typeof(AudioSource)) as AudioSource;
	}
	
	// Update is called once per frame
	void Update () {
        if (this.doRotate)
            this.gameObject.transform.Rotate(new Vector3(0, 0, 1) * this.rotationSpeed);

        if (this.startHitTime + 1.5f < Time.time && this.isHited == true)
            Destroy(this.gameObject);
	}

    public void StartHit()
    {
        if (this.animatorController && !this.isHited)
        {
            if (this.hitAudioSource)
            {
                int i = Random.Range(0, this.randomClips.Length);
                this.hitAudioSource.PlayOneShot(this.randomClips[i]);
            }
            this.animatorController.SetTrigger("hit");
            this.startHitTime = Time.time;
            this.isHited = true;
        }
    }

    
}
