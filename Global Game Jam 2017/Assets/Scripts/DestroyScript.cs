﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyScript : MonoBehaviour {


	public GameObject Parent;
	public HitObject Col1;
	public HitObject Col2;
	public HitObject Col3;


	void OnDestroy(){
		GameController.Instance.ObstacleDestrozed (Col1.number);
		GameController.Instance.ObstacleDestrozed (Col2.number);
		GameController.Instance.ObstacleDestrozed (Col3.number);

		GameObject.Destroy (Parent);
	}
}
