﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light_Pulsation : MonoBehaviour {

	public float Base_Lightintensity = 1;

	public float Pulse = 1;
	public float Pulse_Randomness = 0.1f;

	public float Amplitude = 0.2f;
	public float Amplitude_Randomness = 0.01f;

	public float Sub_Pulse = 0.2f;
	public float Sub_Amplitude = 0.2f;

	private Color lightcolor = Color.white;
	private SpriteRenderer myrenderer;
	private float alpha_value = 1;
	private float degrees = 0;
	private float sub_degrees = 0;
	// Use this for initialization
	void Start () {
		myrenderer = transform.GetComponent<SpriteRenderer>();
		myrenderer.material.color = lightcolor;
	}
	
	// Update is called once per frame
	void Update () {
		Alpha_Calculator();
		lightcolor = new Color(lightcolor.r,lightcolor.g,lightcolor.b,alpha_value);
		myrenderer.material.color = lightcolor;
	}

	public void Set_Light_Color(Color New_Color){
		lightcolor = New_Color;		
		myrenderer.material.color = lightcolor;
	}

	private void Alpha_Calculator()
	{
		float pulserandom_value = Random.Range(-Pulse_Randomness,Pulse_Randomness);
		float amplituderandom_value = Random.Range(-Amplitude_Randomness,Amplitude_Randomness);
		degrees += (Pulse+pulserandom_value);
		sub_degrees += (Sub_Pulse);
		if(degrees>360){degrees-=360;}
		float sinvalue = Mathf.Sin(degrees*Mathf.Deg2Rad);
		float multiplier = ((Amplitude+amplituderandom_value)/Base_Lightintensity);
		float shift_value = Base_Lightintensity-((Amplitude+amplituderandom_value)/Base_Lightintensity);
		float sub_sinvalue = Mathf.Sin(sub_degrees*Mathf.Deg2Rad);
		float sub_multiplier = Sub_Amplitude/Base_Lightintensity;
		alpha_value = (sinvalue*multiplier) + shift_value + (sub_sinvalue*sub_multiplier);

	}

}
