﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {

	public Text level;
	public Text score;
	public Text time;
	public Text wavesUsed;

	public Button nextLevel;

	// Use this for initialization
	void Start () {
		int currentLevel = GameLogic.Instance.getCurrentLevel ();
		level.text = "Level " + currentLevel.ToString();
		wavesUsed.text = GameLogic.Instance.Get_waves_used(currentLevel).ToString();
		nextLevel.onClick.AddListener ( () => {loadNextLevel();} );
	}

	void loadNextLevel() {
		GameLogic.Instance.startNextLevel ();
	}
}
