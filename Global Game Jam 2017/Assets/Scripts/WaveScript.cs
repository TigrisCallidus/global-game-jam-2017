﻿using UnityEngine;
using System.Collections;

// Controlling the shader
public class WaveScript : MonoBehaviour {

	public float Radius;
	public float Angle;
	public float OpenAngle;
	public Segment Seg;


	Material Mat;
	// Use this for initialization
	void Start () {
		Mat = this.gameObject.GetComponent<MeshRenderer> ().material;
		Mat.color = GameController.WaveColor;
		Invoke ("DoDestroy", 10);
	}
	
	// Update is called once per frame
	void Update () {

		float maxLength = Seg.Parent.MaxDistance*Segment.GrowthParameter*4/3;


		Mat.SetFloat ("_Scale",(Radius-1));
		float haloScale=1;
		if (Radius>0) {
			haloScale = 1 - Radius / maxLength;
		}
		this.transform.rotation = Quaternion.Euler (0,180,Angle+180);
		float scaleValue;
		float offSet;
		if (OpenAngle >= 360) {
			scaleValue = 0.8f;
			offSet = 0.1f;
		} else {
			//min angle =6;
			scaleValue = 360 / OpenAngle;
			offSet = -(scaleValue - 1) / 2;
		}
		Mat.SetTextureScale ("_MainTex", new Vector2 (scaleValue, haloScale));
		Mat.SetTextureOffset ("_MainTex", new Vector2 (offSet, 0));

	}

	public void PutOff(){
		this.gameObject.GetComponent<MeshRenderer> ().enabled = false;
		this.enabled = false;
	}

	void DoDestroy(){

		GameObject.Destroy (this.gameObject);
	}
}
