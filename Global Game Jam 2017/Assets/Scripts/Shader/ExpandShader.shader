﻿Shader "Custom/Expand"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		_Scale ("Scale", float) = 1
 		_MainTex ("Color (RGB) Alpha (A)", 2D) = "white"

	}
	SubShader
	{
		Tags {"Queue"="Overlay" "IgnoreProjector"="False" "RenderType"="Transparent"}
		Cull Off
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        //LOD 100


		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag Lambert alpha

			#include "UnityCG.cginc"

			fixed4 _Color;
			float _Scale;
			sampler2D _MainTex;
			fixed4 _MainTex_ST;


			struct appdata
			{
				float4 vertex : POSITION;
				fixed2 uv : TEXCOORD0;
				fixed3 normal: NORMAL;
			};

			struct v2f
			{
				fixed2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			
			v2f vert (appdata v)
			{
				v2f myv2f;
				// Expansion in direction of normals
				v.vertex.xyz+=v.normal*_Scale;

				// needed stuff
				myv2f.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				myv2f.uv=TRANSFORM_TEX(v.uv, _MainTex);

				return myv2f;
			}
			
			fixed4 frag (v2f i) : COLOR
			{


			//transparency from texture
				fixed4 tex = tex2D(_MainTex, i.uv);

				// take color chosen
				fixed4 col=fixed4(_Color.rgb, tex.a);
				return col;
			}
			ENDCG
		}
	}
}
