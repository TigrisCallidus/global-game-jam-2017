﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainUI : MonoBehaviour {

    public GameObject pauseScreenObject;
    public GameObject pauseButtonObject;

    public GameObject pauseScreenCollider;

    public GameObject scoreScreenObject;
    public Image usedWavesZifferOne;
    public Image usedWavesZifferTwo;

    public Sprite[] zifferSprites;


    public void LoadScene(string name)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(name);
    }

    public void PauseGame()
    {
        if (this.pauseButtonObject != null && this.pauseScreenObject != null)
        {
            this.pauseButtonObject.SetActive(false);
            this.pauseScreenObject.SetActive(true);
            this.pauseScreenCollider.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void ResumeGame()
    {
        if (this.pauseButtonObject != null && this.pauseScreenObject != null)
        {
            this.pauseButtonObject.SetActive(true);
            this.pauseScreenObject.SetActive(false);
            this.pauseScreenCollider.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ShowScore(int wavesUsed)
    {
        if (this.scoreScreenObject != null && this.usedWavesZifferOne != null && this.usedWavesZifferTwo != null && this.pauseButtonObject != null)
        {
            this.pauseButtonObject.SetActive(false);
            Time.timeScale = 0f;
            this.scoreScreenObject.SetActive(true);
			if (wavesUsed > 99) {
				this.usedWavesZifferOne.sprite = this.zifferSprites [9];
				this.usedWavesZifferTwo.sprite = this.zifferSprites [9];
			} else {
				int zifferOne = wavesUsed / 10;
				int zifferTwo = wavesUsed % 10;
				if (this.zifferSprites.Length > 9) {
					this.usedWavesZifferOne.sprite = this.zifferSprites [zifferOne];
					this.usedWavesZifferTwo.sprite = this.zifferSprites [zifferTwo];
				}
			}

        }

    }
}
