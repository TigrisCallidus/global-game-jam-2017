﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour {

	public static GameLogic Instance;

	private int[] waves_used = new int[10];
	private int next_level_index; 
	private int currentLevel = 0;

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
	}
	// Use this for initialization
	void Start () {
		Instance = this;
	}

	public void Set_waves_used(int index, int value){
		waves_used[index] = value;
	}

	public int Get_waves_used(int index){
		return waves_used[index];
	}

	public void finishLevelWith(int wavesUsed) {

        print("show score");
        waves_used [currentLevel] = wavesUsed;
        //SceneManager.LoadScene("Score", LoadSceneMode.Single);
        GameObject.FindGameObjectWithTag("basicCanvas").GetComponent<MainUI>().ShowScore(wavesUsed);

	}		

	public void startNextLevel() {
		currentLevel++;
		SceneManager.LoadScene ("menu");
	}
		
	public int getCurrentLevel() {
		return currentLevel;
	}
}
