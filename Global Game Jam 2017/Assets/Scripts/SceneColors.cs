﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneColors : MonoBehaviour {

    public Color foregroundColor;
    public Color backgroundColorOne;
    public Color backgroundColorTwo;
    public Color backgroundColorThree;

    public Color lightColor;

	public Color WaveColor;

    public Color destroyableObjectColor;

    public SceneColors()
    {
        this.foregroundColor = new Color(0.1f, 0.1f, 0.1f, 1);
        this.backgroundColorOne = new Color(0.3f, 0.3f, 0.3f, 1);
        this.backgroundColorTwo = new Color(0.4f, 0.4f, 0.4f, 1);
        this.backgroundColorThree = new Color(0.5f, 0.5f, 0.5f, 1);
        this.lightColor = new Color(1, 1, 1, 0.5f);
        this.destroyableObjectColor = new Color(1, 1, 1, 1);
    }
    // Use this for initialization
    void Start () {
        GameObject[] forgroundObjects = GameObject.FindGameObjectsWithTag("forground");
        foreach (GameObject forgroundObj in forgroundObjects)
        {
            SpriteRenderer spriteRender = forgroundObj.GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (spriteRender != null)
                spriteRender.color = this.foregroundColor;
        }

        GameObject[] backgroundOneObjects = GameObject.FindGameObjectsWithTag("backgroundOne");
        foreach (GameObject backgroundOneObj in backgroundOneObjects)
        {
            SpriteRenderer spriteRender = backgroundOneObj.GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (spriteRender != null)
                spriteRender.color = this.backgroundColorOne;
        }

        GameObject[] backgroundTwoObjects = GameObject.FindGameObjectsWithTag("backgroundTwo");
        foreach (GameObject backgroundTwoObj in backgroundTwoObjects)
        {
            SpriteRenderer spriteRender = backgroundTwoObj.GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (spriteRender != null)
                spriteRender.color = this.backgroundColorTwo;
        }

        GameObject[] backgroundThreeObjects = GameObject.FindGameObjectsWithTag("backgroundThree");
        foreach (GameObject backgroundThreeObj in backgroundThreeObjects)
        {
            SpriteRenderer spriteRender = backgroundThreeObj.GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (spriteRender != null)
                spriteRender.color = this.backgroundColorThree;
        }


        GameObject[] lightObjects = GameObject.FindGameObjectsWithTag("light");
        foreach (GameObject lightObj in lightObjects)
        {
            SpriteRenderer spriteRender = lightObj.GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (spriteRender != null)
                spriteRender.color = this.lightColor;
        }

		GameController.WaveColor = WaveColor;

        GameObject[] destroableObjects = GameObject.FindGameObjectsWithTag("DestroyableObjects");
        foreach (GameObject destroableObj in destroableObjects)
        {
            SpriteRenderer spriteRender = destroableObj.GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            if (spriteRender != null)
                spriteRender.color = this.destroyableObjectColor;
        }
    }
}
